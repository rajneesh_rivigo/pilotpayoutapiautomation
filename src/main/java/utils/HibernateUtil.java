package utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final  SessionFactory sessionFactory;
    static {
        try {

            Configuration cfg = new Configuration();
            cfg.setProperty("hibernate.connection.url", CommonUtil.loadTestData.getString("connection.url"));
            cfg.setProperty("hibernate.connection.username", CommonUtil.loadTestData.getString("connection.username"));
            cfg.setProperty("hibernate.connection.password", CommonUtil.loadTestData.getString("connection.password"));
            cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
            cfg.configure();

            sessionFactory = cfg.buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("SessionFactory creation failed" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }


}
