package utils;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.rivigo.utils.capturing.Config;

import java.util.Properties;

public class SshUtil {

    private Session session = null;

    public void createSshTunnel(String remoteHost,int remotePort)throws JSchException {
        Config userConfig = new Config("src/main/resources/user.properties");
        String sshUser = userConfig.getProperty("SSH_USER");
        String sshPassword = userConfig.getProperty("SSH_PASSWORD");
        String sshHost = userConfig.getProperty("SSH_HOST");
        int localPort = Integer.parseInt(userConfig.getProperty("LOCAL_PORT"));
        String  keyPath = userConfig.getProperty("SSH_KEY_PATH");
        createSshTunnel(sshUser,sshPassword,sshHost,remoteHost,localPort,remotePort,keyPath,22);
    }

    public void createSshTunnel(String strSshUser, String strSshPassword, String strSshHost,
                                    String strRemoteHost, int nLocalPort, int nRemotePort,String keyPath,int sshPort) throws JSchException {
        if(session!=null) {
           disconnect();
        }

        final JSch jsch = new JSch();
        jsch.addIdentity(keyPath);
        session = jsch.getSession(strSshUser, strSshHost, sshPort);
        session.setPassword(strSshPassword);
        final Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
    }

    public void disconnect() {
        if(session!=null)
            session.disconnect();
    }
}
