package utils;

import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import hibernate.CrudOperations;
import org.junit.runner.RunWith;
import org.testng.annotations.*;
import utils.CommonUtil;


@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        features = "src/test/java/features",
      //  plugin = {"pretty", "html:target/cucumber-html-report"},
        glue = {"utils","steps"},
        tags = {"@VerifyPenalties                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     "},
        format = {
                "pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/CucumberTestReport.json",
                "rerun:target/cucumber-reports/rerun.txt"}
)
public class CucumberRunnerUtil {

    private TestNGCucumberRunner testNGCucumberRunner;


    @BeforeSuite
    @Parameters( "environment")
    public void setUpEnvironment(@Optional String environment){
        CommonUtil.environmentSetUp("staging");
    }

    @BeforeClass(alwaysRun = true)
    public void setUpClass() throws Exception {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }


    @Test(groups = "cucumber", description = "Runs Cucumber Feature",dataProvider = "features" )
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        this.testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }


    @After
    public void closeTransaction(Scenario scenario){

        if (scenario.isFailed()){
            CrudOperations.commitAndCloseHibernateConnection();
        }

        CrudOperations.commitAndCloseHibernateConnection();
    }


   // @AfterTest
    public void closeHibernateTransaction(Scenario scenario){

        if (scenario.isFailed()){
            CrudOperations.commitAndCloseHibernateConnection();
        }

        CrudOperations.commitAndCloseHibernateConnection();
    }


    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }



    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        testNGCucumberRunner.finish();
    }


    @AfterSuite
    public void cleanUp(){

        CrudOperations.closeSessionFactory();
    }
}

