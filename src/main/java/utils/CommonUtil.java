package utils;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.rivigo.Log;
import com.rivigo.utils.LogUtil;
import com.rivigo.utils.RestUtil;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.hibernate.Session;
import org.joda.time.DateTime;

import javax.persistence.Query;
import java.util.HashMap;

public class CommonUtil {

    public static Configuration loadTestData ;
    public static RestUtil restUtil;
    public static RestUtil vehicleRestUtil;
    static RequestSpecification pilotBaseUrl;
    static RequestSpecification vehicleBaseUrl;
    static Session session;
    static String enviroment = null;
    HashMap<String, String> getRequestParameters;
    HashMap<String, String> getRequestHeaders;
    String apiURL;
    Response response;
    Query query;
    DateTime pastTime;
    static String redisHostName;
    static int redisPort;

    public CommonUtil() {
        getRequestHeaders = new HashMap<>();
        getRequestParameters = new HashMap<>();
    }


    public static void environmentSetUp(String environment) {
        enviroment = environment;
        loadTestData = loadPropertiesFile(enviroment);
        setBaseUrl(enviroment);
        restUtil = new RestUtil(pilotBaseUrl);
        vehicleRestUtil = new RestUtil(vehicleBaseUrl);
    }

    public static void setBaseUrl(String environment) {
        pilotBaseUrl = new RequestSpecBuilder().setBaseUri(loadTestData.getString("pilotBaseUrl")).build();
        Log.info("piloturl"+ pilotBaseUrl);
       // vehicleBaseUrl = new RequestSpecBuilder().setBaseUri(loadTestData.getString("vehicleBaseUrl")).build();
        redisHostName = loadTestData.getString("host");
        redisPort = loadTestData.getInt("port");
    }

    public static Configuration loadPropertiesFile(String filename) {
        Parameters params = new Parameters();
        FileBasedConfigurationBuilder<FileBasedConfiguration> builder =
                new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                        .configure(params.properties()
                                .setFileName(filename + ".properties"));
        try {
            Configuration config = builder.getConfiguration();
            return builder.getConfiguration();
        } catch (ConfigurationException cex) {
            // loading of the configuration file failed
            LogUtil.info(filename + " is not loaded" + cex.getMessage());
            return null;
        }

    }
/*
    @Before
    public void baseSetUp() {
        //   Log.info("Base url: " + pilotBaseUrl.get());
        initialSetUp();
    }

    @After
    public void closeHibernateTransaction() {

        //CrudOperations.commitAndCloseHibernateConnection();
    }

    *//* select all the pilots (role=2) from User table and mark them inactive.*//*
    public void initialSetUp() {
       // session = CrudOperations.createHibernateConnection();
        String inactiveAllPilotsQuery = "update User u set u.isActive=0" +
                "where u.role = :role and u.isActive= :isActive";

        query = session.createQuery(inactiveAllPilotsQuery)
                .setParameter("role", 2)
                .setParameter("isActive", true);

        int result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on User table");


        // set foreign key constraint to 0
        query = session.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully for foreign key check (0)");

        // deleting all records of pilot activity
        query = session.createQuery("delete  from PilotActivity");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pilot activity");

        // deleting all records of pilot activity plan
        query = session.createQuery("delete  from PilotActivityPlan");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pilot activity plan");

        // deleting all records of pilot activity history
        query = session.createQuery("delete  from PilotActivityHistory ");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pilot activity history ");

        // deleting all records of pilot action
        query = session.createQuery("delete  from PilotAction");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pilot action");

        // deleting all records of pilot duty
        query = session.createQuery("delete  from PilotDuty");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pilot duty");

        // deleting all records of pilot penalty
        query = session.createQuery("delete  from PilotPenalty");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pilot penalty");

        // deleting all records of pse action log
        query = session.createQuery("delete  from PseActionLog ");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pse action log");


        // deleting all records of pilot leave
        query = session.createQuery("delete  from PilotLeave ");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pilot leave");

        // deleting all records of pilot allocation change log
        query = session.createQuery("delete  from PilotAllocationChangeLog");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on pilot allocation change log");

        // deleting all records of unallocated tracking logs
        query = session.createQuery("delete  from UnallocatedTrackingsLog");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on unallocated tracking log");


        // set foreign key constraint back to 1
        query = session.createNativeQuery("SET FOREIGN_KEY_CHECKS =1");
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully for foreign key check (1) ");

        // setting all vehicles ETD in past
        long currentTime = DateTime.now().getMillis() - (24*3600000);
        DateTime currTime = new DateTime(currentTime);
        pastTime = new DateTime(currentTime - 7200000);
        query = session.createQuery("update VehicleNodeTracking v set v.etd=:etd, v.actualOutTimestamp = null, " +
                "v.gpsOutTimestamp = null, v.manualOutTimestamp = null, v.actualInTimestamp = null, " +
                "v.manualInTimestamp = null, v.gpsInTimestamp = null, v.pilotInTimestamp = null," +
                "v.pilotOutTimestamp = null, v.inPilotId = null, v.outPilotId = null where v.etd > :currentTime ")
                .setParameter("etd", pastTime).setParameter("currentTime", currTime);
        result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully on vehicle node tracking");

        // CrudOperations.commitAndCloseHibernateConnection();

        //Flush redis data
        Jedis jedis = new Jedis(redisHostName, redisPort);
        jedis.flushAll();
    }*/
}
