package utils;


import com.google.gson.Gson;
import com.jcraft.jsch.JSchException;
import com.rivigo.utils.capturing.Config;
import enums.DbHosts;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MysqlDbUtil {

    private Connection connection = null;
    private SshUtil ssh = null;
    private MysqlDbUtil(){}

    public static MysqlDbUtil getInstance(String dbName, DbHosts host, boolean useSsh){
        MysqlDbUtil mysql = new MysqlDbUtil();
        mysql.setDbConnection(dbName,host,useSsh);
        return mysql;
    }

    public static MysqlDbUtil getInstance(String dbName, String userName, String password, String host, int port, boolean useSsh){
        MysqlDbUtil mysql = new MysqlDbUtil();
        mysql.setDbConnection(dbName,userName,password,host,port,useSsh);
        return mysql;
    }

    private void setDbConnection(String dbName, DbHosts host, boolean useSsh) {
        Config userConfig = new Config("src/main/resources/user.properties");
        setDbConnection(dbName,userConfig.getProperty(host.name()+"DBUSER"),userConfig.getProperty(host.name()+"DBPASSWORD"),host.hostName(),host.port(),useSsh);
        //setDbConnection(dbName,host,useSsh);
    }

    private void setDbConnection(String dbName,String userName,String password,String host,int port,boolean useSsh) {

        String localPort = "";
        if(useSsh) {
            try {
                if(ssh!=null)
                    ssh.disconnect();
                ssh = new SshUtil();
                ssh.createSshTunnel(host,port);
                localPort = new Config("src/main/resources/user.properties").getProperty("LOCAL_PORT");
            } catch (JSchException e) {
                e.printStackTrace();
            }
        }

        if(connection!=null)
            disconnect();

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://"+(useSsh?"localhost":host)+":"+(useSsh?localPort:port)+"/"+dbName , userName,password);
        } catch (SQLException|ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ResultSet getDataFromDb(String query){
        ResultSet result = null;
        if(connection!=null) {
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                result = statement.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public String getJsonDataFromDb(String query){
        List<Map<String,String>> jsonResult = new ArrayList<>();

        if(connection !=null) {
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                if (extractData(jsonResult, statement)) return new Gson().toJson(jsonResult);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    private boolean extractData(List<Map<String, String>> jsonResult, PreparedStatement statement) {
        try(ResultSet result = statement.executeQuery()){
            while (result.next()) {
                Map<String, String> row = new HashMap<>();
                ResultSetMetaData metaData = result.getMetaData();
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    row.put(metaData.getColumnLabel(i), result.getString(i));
                }
                if (!row.isEmpty())
                    jsonResult.add(row);
            }

            return true;
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void disconnect(){
        if(connection!=null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(ssh!=null)
            ssh.disconnect();
    }


}
