package hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import utils.HibernateUtil;

abstract public class CrudOperations {

    static SessionFactory sessionFactory;
    static Session session;

    public static Session createHibernateConnection(){

                     if (sessionFactory == null) {
                    //Create session factory object
                    sessionFactory = HibernateUtil.getSessionFactory();
                }
            //getting session object from session factory
            session = sessionFactory.openSession();
            //getting transaction object from session object
            session.beginTransaction();

        return  session;
    }


    public static void commitAndCloseHibernateConnection(){
        if( session.isOpen()) {
            session.getTransaction().commit();
            session.close();
        }
    }

    public static void closeSessionFactory(){
        if (sessionFactory!= null && sessionFactory.isOpen()) {
            sessionFactory.close();
        }
    }


}
