package hibernate;

import org.hibernate.HibernateException;
import org.hibernate.annotations.TypeDef;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.usertype.UserType;
import org.joda.time.DateTime;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by anant on 15/3/16.
 */
@MappedSuperclass
@TypeDef(defaultForType = DateTime.class, typeClass = JodaDateTimeAsMillis.class)
public class JodaDateTimeAsMillis implements UserType/*, UserVersionType*/{
    @Override
    public int[] sqlTypes() {
        return new int[]{Types.BIGINT};
    }

    @Override
    public Class<DateTime> returnedClass() {
        return DateTime.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if ((x == null) && (y == null)) {
            return true;
        }
        if ((x == null) || (y == null)) {
            return false;
        }
        return x.equals(y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        assert (x != null);
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {
        Object instant = StandardBasicTypes.LONG.nullSafeGet(rs, names, session, owner);
        return null != instant ? new DateTime(instant) : null;

    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
        StandardBasicTypes.LONG.nullSafeSet(st, null != value ? ((DateTime) value).getMillis() : null, index, session);
    }



    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }

    /*@Override
    public Object seed(SessionImplementor sessionImplementor) {
        return new DateTime();
    }

    @Override
    public Object next(Object o, SessionImplementor sessionImplementor) {
        return new DateTime();
    }

    @Override
    public int compare(Object o, Object t1) {

        if(((DateTime) o).getMillis() < (((DateTime) t1).getMillis())){
            return -1;
        }else if (((DateTime) o).getMillis() > (((DateTime) t1).getMillis())){
            return 1;
        }else {
            return 0;
        }
    }*/
}
