package hibernate;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;

@Getter
@MappedSuperclass
public class AbstractEntity extends JodaDateTimeAsMillis implements Serializable {
    private static final long serialVersionUID = -3649285369162252355L;
    @Setter
    @Column(nullable = false, name = "created_timestamp")
    protected DateTime createdTimestamp;

    @Setter
    @Column(nullable = false, name = "last_updated_timestamp")
    protected DateTime lastUpdatedTimestamp;

    @Setter
    @Column(name = "created_by")
    protected Long createdBy;

    @Setter
    @Column(name = "last_updated_by")
    protected Long lastUpdatedBy;

    @Setter
    @Column(nullable = false, name = "is_active")
    protected Boolean isActive;

    @PrePersist
    protected void onCreate() {
        this.lastUpdatedTimestamp = this.createdTimestamp = DateTime.now();
        this.isActive = null == this.isActive ? true : this.isActive;
       /* if (this.createdBy == null && null != getSessionUserId()) {
            this.lastUpdatedBy = this.createdBy = getSessionUserId();
        }*/
    }

    @PreUpdate
    protected void onUpdate() {
        this.lastUpdatedTimestamp = DateTime.now();
     /*   if(null != getSessionUserId()) {
            this.lastUpdatedBy = getSessionUserId();
        }*/
    }


  /*  private Long getSessionUserId(){
        String username = null;
        if (SecurityContextHolder.getContext() != null) {
            if (SecurityContextHolder.getContext().getAuthentication() == null) {
                return null;
            }
            try {
                username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            } catch (Exception e) {
                log.warn("Couldn't get user name from the session.",e);
            }
        }
        if (username != null) {
            return ApplicationContextAwareConfig.getUserId(username);
        }
        return null;
    }*/
}
