package enums;

/**
 * Created by priyaranjan on 12/12/16.
 */
public enum PilotRole {
    PRIMARY,
    DOUBLE_DRIVER,
    DRY_RUN
}
