package enums;

import java.util.Arrays;
import java.util.List;

/**
 * Created by priyaranjan on 24/11/16.
 */
public enum ActivityStatus {

    PLANNED,
    IN_PROGRESS,
    COMPLETED,
    MISSED,
    DELETED;

    public static List<ActivityStatus> yetToCompleteStatusList() {
        return Arrays.asList(PLANNED, IN_PROGRESS);
    }
}
