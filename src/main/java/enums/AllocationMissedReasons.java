package enums;

/**
 * Created by user on 20/3/17.
 */
public enum AllocationMissedReasons {

    SUITABLE_PILOT_UNAVAILABLE,         // could not find suitable pilot
    ALREADY_MANUALLY_ASSIGNED,          // vehicle already left pitstop
    LAST_PITSTOP,                       // assignment not allowed in last pitstop
    ETA_IN_PAST,                        // eta < current time
    ETD_IN_PAST,                        //ETD < current time
    HUB_UNAVAILABLE,                    // auto-allocation not running on this hub probably
    VEHICLE_NOT_LEFT_PREVIOUS_NONPITSTOP,
    MTRF_VEHICLE,
    NOT_ATTEMPTED_YET,
    TRIP_NOT_STARTED_FROM_CWH,
    INCORRECT_ASSIGNMENT_ATTEMPT,

    VEHICLE_ALREADY_REACHED_NEXT_LOCATION,
    VEHICLE_TRIP_ALREADY_CLOSED,
    VEHICLE_ALREADY_LEFT_PITSTOP
}
