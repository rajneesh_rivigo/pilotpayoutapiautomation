package enums;

/**
 * Created by puneet on 14/2/17.
 */
public enum PseActionType {
    DUTY_START,
    DUTY_ACCEPT,

    ACTIVITY_START,
    ACTIVITY_END,
    DUTY_END
}
