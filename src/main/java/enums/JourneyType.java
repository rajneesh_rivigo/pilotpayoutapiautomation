package enums;

import lombok.Getter;

@Getter
public enum JourneyType {

    TRIP, NON_TRIP;

    public String initials() {
        switch (this) {
            case TRIP:
                return "T";
            case NON_TRIP:
                return "NT";
        }
        return "U";
    }
}
