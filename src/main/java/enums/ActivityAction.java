package enums;

/**
 * Created by user on 6/1/17.
 */
public enum ActivityAction {
    /**
     * The word "Updation" is not recognized either by Oxford dictionary or by Merriam Webster dictionary,
     * neither for US English nor for world english. Also, it is incorrect to say that
     * "updation" is used mainly in India ONLY as a substitute to "updating" - wiktionary.org
     */
    CREATION,   /** on pilot allocation*/
    DELETION,   /** on pilot deallocation*/
    UPDATE,   /** when activitu is update */
    RESET       /** used when the entire-duty is reset.*/
}
