package enums;

/**
 * Created by sowmya on 3/1/18.
 */
public enum TripStatus {
    UNPLANNED(2), PLANNED(2), PLACED(2), RUNNING(2), CLOSED(1);

    private int sortPriority;

    TripStatus(int sortPriority) {
        this.sortPriority = sortPriority;
    }

}
