package enums;

import com.rivigo.utils.capturing.Config;

public enum DbHosts {
    LOCAL,STAGING;

    public String hostName()
    {
        Config userConfig = new Config("src/main/resources/user.properties");
        return userConfig.getProperty(this.name()+"_DB");
    }
    public int port()
    {
        Config userConfig = new Config("src/main/resources/user.properties");
        return Integer.parseInt(userConfig.getProperty(this.name()+"_DB_PORT"));
    }
}
