package enums;

public enum LeaveStatus {
    PENDING,
    APPROVED,
    REJECTED
}
