package enums;

/**
 * Created by priyaranjan on 24/11/16.
 */
public enum PilotDutyState {
    PLANNED,
    IN_PROGRESS,
    COMPLETED,
    RESET,
    MISSED;

   /* public static PilotDerivedState getPilotDerivedState(PilotDutyState dutyState,
                                                         PilotState pilotState,
                                                         ActivityStatus activityStatus,
                                                         Boolean dutyConfirmed) {
        switch (dutyState) {
            case PLANNED:
                if (dutyConfirmed){
                    return PilotDerivedState.ACCEPTED;
                }else {
                    return PilotDerivedState.PENDING;
                }
            case IN_PROGRESS:
                switch (activityStatus){
                    case PLANNED:
                        switch (pilotState){
                            case RUNNING_ON_TRIP:
                            case DOUBLE_DRIVER:
                            case WAITING_AT_LATLONG:
                            case PILOT_DRY_RUN: return PilotDerivedState.ACCEPTED;
                            case WAITING_AT_NODE: return PilotDerivedState.AT_PITSTOP;
                        }
                    case IN_PROGRESS: return PilotDerivedState.RUNNING_ON_TRIP;
                    case COMPLETED: return PilotDerivedState.COMPLETED;
                }
                return PilotDerivedState.AT_PITSTOP;
            case COMPLETED:
                return PilotDerivedState.COMPLETED;
            case MISSED:
                return PilotDerivedState.DECLINED;
        }
        return PilotDerivedState.PENDING;
    }*/
}
