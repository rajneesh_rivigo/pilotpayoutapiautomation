package enums;

/**
 * Created by priyaranjan on 24/11/16.
 */
public enum ActivityType {
    DRY_RUN, // Pilot dry run
    TRIP
}
