package enums;

public enum HistoryTriggerSource {
    VEHICLE, PILOT_APP, PILOT_APP_GPS, PSE, SYSTEM, IVRS_CALL
}

