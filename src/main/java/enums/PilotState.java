package enums;

public enum PilotState {
    REST,
    LEAVE,
    WAITING_AT_LATLONG,
    WAITING_AT_NODE,
    // VEHICLE_DRY_RUN,
    RUNNING_ON_TRIP,
    PILOT_DRY_RUN,
    DOUBLE_DRIVER
}
