package enums;

/**
 * Created by priyaranjan on 24/11/16.
 */
public enum PenaltyType {
    NO_PENALTY,
    NOT_CONFIRMED,
    NOT_REPORTED,
    LATE_REPORTED,
    ASSET_JACK_MISSING,
    ASSET_STEPNEY_MISSING
}
