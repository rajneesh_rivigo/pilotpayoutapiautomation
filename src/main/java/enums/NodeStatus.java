package enums;

/**
 * Created by priyaranjan on 25/11/16.
 */
public enum NodeStatus {
    NOT_REACHED,
    REACHED,
    LEFT
}
