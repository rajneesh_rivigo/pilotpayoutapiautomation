package enums;

public enum NodeType {
    CLIENT_WAREHOUSE,
    PITSTOP,
    FUEL_PUMP,
    PARKING_LOT,
    CHECKPOST,
    TOLL,
    WORKSHOP;

    private NodeType() {
    }
}
