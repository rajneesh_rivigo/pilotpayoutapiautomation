package steps;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.rivigo.utils.AssertionUtil;
import com.rivigo.utils.LogUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import hibernate.CrudOperations;
import org.apache.commons.configuration2.Configuration;
import org.hibernate.Session;
import org.joda.time.DateTime;
import utils.CommonUtil;

import javax.persistence.Query;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class CommonSteps {

    HashMap<String,String> getRequestParameters;
    HashMap<String,String> getRequestHeaders;
    static RequestSpecification pilotBaseUrl;
    static RequestSpecification vehicleBaseUrl;
    static Configuration loadTestData;
    String apiURL;
    Response response;
    static Session session;
    Query query;
    DateTime time;
    String enviroment = "local";
    Long assignedPilotId;

    public CommonSteps(){
        getRequestHeaders = new HashMap<>();
        getRequestParameters = new HashMap<>();
    }

    public static void main(String[] args) {
        System.out.println(new DateTime().plusMinutes(60));
    }

    @When("^AA scheduler executes$")
    public void executeAAScheduler(){

        apiURL = "test/testSchedule";
        Response response  = CommonUtil.restUtil.getRequest(apiURL,getRequestHeaders,getRequestHeaders);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static long timeInMillis(int timeInMuinutes){

        long timeInMillis = timeInMuinutes*60*60*1000;

        return timeInMillis;
    }
    /*@When("^Check \"([^\"]*)\" pilots in databse$")
    public void checkPilotsInDatabse(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions

        session = CrudOperations.createHibernateConnection();
        String selectActivePilots = "select User p = :selectValue where p.isActive = 1";
        query = session.createQuery(selectActivePilots);


        List result = query.getResultList();
        LogUtil.info(result + " rows fetched successfully");
        CrudOperations.commitAndCloseHibernateConnection();

        }
*/
    @And("^MPS dry runs are enabled on \"([^\"]*)\"$")
    public void mpsDryRunsAreEligibleFrom(String pitstops){
        session = CrudOperations.createHibernateConnection();
        String setDryRunLocations = "update PrimeProperty p set p.variableValue = :updateValue where p.variableName = 'MPS_DRY_RUN_HUB_LIST' and p.isActive = true";
        query = session.createQuery(setDryRunLocations)
                .setParameter("updateValue", pitstops);

        int result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully");
        CrudOperations.commitAndCloseHibernateConnection();

        apiURL = "test/clear_cache/primePropertySpringCache";
        Response response  = CommonUtil.restUtil.postRequest(apiURL, new HashMap<>(), new HashMap<>());
        AssertionUtil.verifyEquals(response.path("status"), "SUCCESS", "Cache invalidated");
    }


    @Given("^Sectional TAT from (\\d+) \"([^\"]*)\" to (\\d+) \"([^\"]*)\" is (\\d+) hours$")
    public void sectionalTATFromToIsHours(int startNodeId, String startNodeType, int endNodeId, String endNodeType, int tat) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        String startEndNodeHash = startNodeType.concat("_").concat(String.valueOf(startNodeId))
                .concat("_").concat(endNodeType).concat("_").concat(String.valueOf(endNodeId));

        session = CrudOperations.createHibernateConnection();

        String queryString = "update HopPolyline hp set hp.deltaMillis = :tat where hp.startEndNodeHash = :startEndNodeHash";
        query =  session.createQuery(queryString)
                .setParameter("tat",(tat*3600*1000l))
                .setParameter("startEndNodeHash", startEndNodeHash);

        int result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully");
        CrudOperations.commitAndCloseHibernateConnection();

    }

    @Given("^Sectional distance from (\\d+) \"([^\"]*)\" to (\\d+) \"([^\"]*)\" is (\\d+) kms")
    public void sectionalDistanceFromToIsKms(int startNodeId, String startNodeType, int endNodeId, String endNodeType, int distance) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        String startEndNodeHash = startNodeType.concat("_").concat(String.valueOf(startNodeId))
                .concat("_").concat(endNodeType).concat("_").concat(String.valueOf(endNodeId));

        session = CrudOperations.createHibernateConnection();

        String queryString = "update HopPolyline hp set hp.distanceInMeters = :distance where hp.startEndNodeHash = :startEndNodeHash";
        query =  session.createQuery(queryString)
                .setParameter("distance",(distance*1000l))
                .setParameter("startEndNodeHash", startEndNodeHash);

        int result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully");
        CrudOperations.commitAndCloseHibernateConnection();

    }

    @And("^Dynamic route deviation for \"([^\"]*)\" client is (\\d+) kms$")
    public void setDynamicRouteDeviation(String clienttype, int deviation){

        String variableName;
        if(clienttype.equalsIgnoreCase("industrial")){
            variableName = "DYNAMIC_ROUTE_DEVIATION_FOR_INDUSTRIAL_CLIENT";
        }
        else{
            variableName = "DYNAMIC_ROUTE_DEVIATION_FOR_NON_INDUSTRIAL_CLIENT";
        }
        session = CrudOperations.createHibernateConnection();
        String queryString = "update PrimeProperty p set p.variableValue = :updateValue where p.variableName = :variableName and p.isActive = true";
        query = session.createQuery(queryString)
                .setParameter("variableName", variableName)
                .setParameter("updateValue", String.valueOf(deviation*1000L));

        int result = query.executeUpdate();
        LogUtil.info(result + " rows updated successfully");
        CrudOperations.commitAndCloseHibernateConnection();

        apiURL = "test/clear_cache/primePropertySpringCache";
        Response response  = CommonUtil.restUtil.postRequest(apiURL, new HashMap<>(), new HashMap<>());
        AssertionUtil.verifyEquals(response.path("status"), "SUCCESS", "Cache invalidated");
    }
}
