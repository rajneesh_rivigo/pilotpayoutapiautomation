package steps;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.rivigo.utils.AssertionUtil;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dto.FilterDTOs;
import dto.Verification;
import enums.DbHosts;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.util.Strings;
import utils.CommonUtil;
import utils.MysqlDbUtil;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

public class Test {
    private HashMap <String, String> getRequestParameters = new HashMap <>();
    private HashMap <String, String> getRequestHeaders = new HashMap <>();
    String baseurl = CommonUtil.loadTestData.getString("pilotBaseUrl");
    private static final String URI = "pilot/payout/transaction/all";
    private static final String TRANSANCTION = "transactionType";
    private static final String PERIOD = "payoutPeriodId";
    private static final String CONTENT = "Content-Type";
    private static final String APP = "application/json";
    private static final String PAYOUTTYPE = "payoutType";
    private static final String MONTHLY = "MONTHLY_PAYOUT";
    private static final String PAYOUTSTATUS = "status";
    private static final String USER = "1234";
    private static final String PILOTID = "pilotId";
    private static final String PENALTYTYPE = "penaltyType";
    private static final String PAYOUTID = "payoutId";
    private static final String STATUS = "status";
    private static final String SUCCESS = "SUCCESS";
    private static final String ERROR = "errorMessage";
    private Response response;
    private String pilotPayoutId;
    private String penaltyAmount;
    private String transactionIds;
    private String dna;
    private Double lateReportingFromAPI;
    private Double lateReportingFromDB;
    private String notReported;
    private List <String> payDays;
    private List <String> extractedApiResponse;
    private String pilotIds;
    private List <Double> amounts;
    private Double dnaPenaltyAmount = 0.0;
    private Double notReportedPenaltyAmount = 0.0;
    private Long startTime = null;
    private Long endTime = null;
    private int isPenaltyStartedInLastMonth = 0;
    private GetStartAndEndDateOfAPeriod perioddate = new GetStartAndEndDateOfAPeriod();


    @Given("^apply \"([^\"]*)\" and \"([^\"]*)\" filters for \"([^\"]*)\" payout of PeriodId \"([^\"]*)\"$")
    public void applyFilters(String transactionType, String status, String payoutType, int periodId) throws Throwable {
        String URI = "/pilot/payout/transaction/all";
        List <String> payoutStatusFromAPI;
        FilterDTOs body = new FilterDTOs();
        body.setTransactionType(transactionType);
        body.setPayoutType(payoutType);
        body.setStatus(status);
        body.setPayoutPeriodId(periodId);
        getRequestHeaders.put(USER, "1234");
        getRequestHeaders.put(CONTENT, APP);
        ObjectMapper objectMapper = new ObjectMapper();
        response = CommonUtil.restUtil.postJSONRequest(URI, objectMapper.writeValueAsString(body), getRequestHeaders);
        //fetching pilotIds from the response*/
        List <Long> fetchPilotIds = response.path("response.objects.pilotId");
        pilotIds = fetchPilotIds.toString().replace("[", "").replace("]", "");
        extractedApiResponse = response.path("response.objects.pilotPayoutId");
        pilotPayoutId = extractedApiResponse.toString().replace("[", "").replace("]", "");
        payoutStatusFromAPI = response.path("response.objects.status");
        amounts = response.path("response.objects.payoutAmount"); //using it in other functions
    }

    @Given("^apply \"([^\"]*)\" and \"([^\"]*)\" filters for \"([^\"]*)\" payout of PeriodId \"([^\"]*)\" for verifying penalties$")
    public void verifyAllPenalties(String transactionType, String status, String payoutType, int periodId) throws Throwable {
        String URI = "/pilot/payout/transaction/all??&page={pageNumber}";
        List <String> payoutStatusFromAPI;
        int totalPages = 0;
        FilterDTOs body = new FilterDTOs();
        body.setTransactionType(transactionType);
        body.setPayoutType(payoutType);
        body.setStatus(status);
        body.setPayoutPeriodId(periodId);
        getRequestHeaders.put(USER, "1234");
        getRequestHeaders.put(CONTENT, APP);


        for (int a = 0; a <= Integer.valueOf(totalPages); a++) {
            System.out.println("executing page number " + a);
            ObjectMapper objectMapper = new ObjectMapper();
            response = CommonUtil.restUtil.postJSONRequest(baseurl + URI.replace("{pageNumber}", String.valueOf(a)), objectMapper.writeValueAsString(body), getRequestHeaders);
            //fetching pilotIds from the response
            List <Long> fetchPilotIds = response.path("response.objects.pilotId");
            pilotIds = fetchPilotIds.toString().replace("[", "").replace("]", "");
            //Fetching total number of pages from the response
            totalPages = response.path("response.totalPages");
            int pageSize = response.path("response.pageSize");
            //Fetching pilot PayoutIDs from the response
            List <Long> fetchPilotPayoutIds = response.path("response.objects.pilotPayoutId");
            pilotPayoutId = fetchPilotPayoutIds.toString().replace("[", "").replace("]", "");
            payoutStatusFromAPI = response.path("response.objects.status");
            amounts = response.path("response.objects.payoutAmount"); //using it in other functions

            for (int i = 0; i < pageSize; i++) {
                System.out.println(i);
                verifyDurationWisePenaltiesCalculationLogic(String.valueOf(fetchPilotIds.get(i)), String.valueOf(periodId));
                viewPenaltiesComponentsWise(String.valueOf(fetchPilotPayoutIds.get(i)), String.valueOf(transactionType));
                verifyPenalityAmountFromDatabase(String.valueOf(fetchPilotPayoutIds.get(i)), "LATE_REPORTED_PENALTY");
                //Asserting LateReporting values
                AssertionUtil.verifyEquals(lateReportingFromAPI, lateReportingFromDB, "Late reporting penalty is not mathcing for pilotId : " + String.valueOf(fetchPilotIds.get(i)));
                System.out.println("LateReporting penalty for pilot id " + fetchPilotIds.get(i) + " from database is: " + lateReportingFromDB);
                System.out.println("LateReporting penalty for pilot id " + fetchPilotIds.get(i) + " from API is: " + lateReportingFromAPI);
                //Asserting DNA values
                AssertionUtil.verifyEquals(String.valueOf(dna), String.valueOf(dnaPenaltyAmount), "Dna is not Not Matching for pilotId : " + String.valueOf(fetchPilotIds.get(i)));
                System.out.println("DNA penalty for pilot id " + fetchPilotIds.get(i) + " from database is: " + dnaPenaltyAmount);
                System.out.println("DNA penalty for pilot id " + fetchPilotIds.get(i) + " from API is: " + dna);
                //Asserting NotReported values
                AssertionUtil.verifyEquals(String.valueOf(notReported), String.valueOf(notReportedPenaltyAmount), "Not reported penalty amount is not matched for pilotId: " + String.valueOf(fetchPilotIds.get(i)));
                System.out.println("Not Reported penalty for pilot id " + fetchPilotIds.get(i) + " from database is: " + notReportedPenaltyAmount);
                System.out.println("Not Reported penalty for pilot id " + fetchPilotIds.get(i) + " from API is: " + notReported);
                dnaPenaltyAmount = 0.0;
                isPenaltyStartedInLastMonth = 0;
                lateReportingFromAPI = 0.0;
                notReportedPenaltyAmount = 0d;
            }
        }
    }

    @And("^Verify Payout amount from database for \"([^\"]*)\"$")
    public void verifyPayoutAmountFromDatabase(String transactionType) throws Throwable {
        String queryToVerifyPayoutAmount = "select total_amount as totalAmount from pilot_payout_transaction where pilot_payout_id in ({payoutId}) and transaction_type = '{transactionType}'";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String payoutAmountFromDB = mysqlDbUtil.getJsonDataFromDb(queryToVerifyPayoutAmount.replace("{payoutId}", String.valueOf(pilotPayoutId)).replace("{transactionType}", String.valueOf(transactionType)));
        mysqlDbUtil.disconnect();
        TypeToken <List <Verification>> token = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(payoutAmountFromDB, token.getType());
        List <String> dbResult = verificationList.stream().map(ver -> ver.getTotalAmount()).collect(Collectors.toList());
        for (int i = 0; i < dbResult.size(); i++) {
            AssertionUtil.verifyEquals(String.valueOf(Math.round(Double.parseDouble(dbResult.get(i)))), String.valueOf(amounts.get(i)), "Not matched");
            System.out.println("Payout Amount from Database is: " + Math.round(Double.parseDouble(dbResult.get(i))) + " Payout amount from API: " + amounts.get(i));
        }

    }


    @Then("^For above Pilot_payout_ids change status to \"([^\"]*)\" for \"([^\"]*)\"$")
    public void approveHoldPayout(String newStatus, String transaction) throws Throwable {
        String apiUrl = "pilot/payout/transaction/updatestatus";
        String body;
        body = new Gson().toJson(extractedApiResponse);
        if (body.equals("null")) {
            System.err.println("body is blank");
        } else {
            System.out.println("body " + body);
            response = given()
                    .contentType(ContentType.JSON)
                    .header("user", 1233)
                    .header(CONTENT, APP)
                    .param("newStatus", newStatus)
                    .param("comments", "Automation_TEST")
                    .param(TRANSANCTION, transaction)
                    .param(PAYOUTTYPE, MONTHLY)
                    .body(body)
                    .when()
                    .put(baseurl + apiUrl)
                    .then()
                    .statusCode(200)
                    .extract().response();
            AssertionUtil.verifyEquals(response.path(STATUS), SUCCESS, "Status is not mathcing");
            AssertionUtil.verifyEquals(response.path(ERROR), "", "There is an error");
            System.out.println("response.asString()  = " + response.asString());
            List <Map <String, String>> statusList = response.path("response");
            List <String> transactionIds = new ArrayList <>();
            for (Map <String, String> element : statusList) {
                transactionIds.add(new ArrayList <>(element.keySet()).get(0));
            }
            String[] result = transactionIds.toArray(new String[transactionIds.size()]);
            this.transactionIds = Strings.join(",", result);
        }
    }


    @Given("^Payout amount of \"([^\"]*)\" pilot for \"([^\"]*)\" and payout period \"([^\"]*)\" for \"([^\"]*)\"$")
    public void payoutAmountForAPilot(String empCode, String transactionType, String periodId, String payoutType) throws Throwable {
        /*Integer payoutAmountFromAPI;
        List<String> pilotPayoutIds;*/
        getRequestHeaders.put(CONTENT, APP);
        getRequestHeaders.put("user", "323");
        getRequestParameters.put(PERIOD, periodId);
        getRequestParameters.put(TRANSANCTION, transactionType);
        getRequestParameters.put(PAYOUTTYPE, payoutType);
        getRequestParameters.put("partialPilotCode", empCode);
        response = CommonUtil.restUtil.getRequest(URI, getRequestParameters, getRequestHeaders);
       /* payoutAmountFromAPI = response.path( "response.objects.payoutAmount" );
        pilotPayoutIds = response.path( "response.objects.pilotPayoutId" );*/
    }


    @Given("^Search for pilot code \"([^\"]*)\" for \"([^\"]*)\" with status \"([^\"]*)\" and payout type \"([^\"]*)\" and PeriodId \"([^\"]*)\"$")
    public void searchPilotCode(int partialPilotCode, String transactionType, String status, String payoutType, int periodId) throws Throwable {
        FilterDTOs body = new FilterDTOs();
        body.setTransactionType(transactionType);
        body.setPayoutType(payoutType);
        body.setStatus(status);
        body.setPartialPilotCode(partialPilotCode);
        body.setPayoutPeriodId(periodId);
        getRequestHeaders.put(CONTENT, APP);
        getRequestHeaders.put("user", "333");
        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(objectMapper.writeValueAsString(body));
        response = CommonUtil.restUtil.postJSONRequest(URI, objectMapper.writeValueAsString(body), getRequestHeaders);
        System.out.println("response of " + partialPilotCode + " is : " + response.asString());
        AssertionUtil.verifyEquals(response.path("status"), "SUCCESS", "Not Mathced");
        extractedApiResponse = response.path("response.objects.pilotPayoutId");
        pilotPayoutId = extractedApiResponse.toString().replace("[", "").replace("]", "");
        amounts = response.path("response.objects.payoutAmount");
        payDays = response.path("response.objects.payDays");
        List <String> result = new ArrayList <>();
        payDays.stream().forEach(x -> {
            result.add(x.substring(0, x.indexOf("days")).trim());
        });
        payDays.clear();
        payDays.addAll(result);
        System.out.println("payDays = " + payDays);

    }

    @And("^Verify Payout status from database is \"([^\"]*)\" and \"([^\"]*)\"$")
    public void verifyStatusFromDatabase(String status, String transactionType) throws Throwable {

        String queryForPayoutStatus = "select pilot_payout_id, status from pilot_payout_transaction where pilot_payout_id in ({payoutId}) and transaction_type = '{transactionType}'";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String jsonResult = mysqlDbUtil.getJsonDataFromDb(queryForPayoutStatus.replace("{payoutId}", String.valueOf(pilotPayoutId)).replace("{transactionType}", String.valueOf(transactionType)));
        mysqlDbUtil.disconnect();
        TypeToken <List <Verification>> token = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(jsonResult, token.getType());
        List <String> dbResult = verificationList.stream().map(ver -> ver.getStatus()).collect(Collectors.toList());
        for (int a = 0; a < dbResult.size() - 1; a++) {
            AssertionUtil.verifyEquals(status, dbResult.get(a), "Its not Matching");
            System.out.println("Status from database is :" + status + " and Status from api is: " + dbResult.get(a));
        }
    }

    @And("^View the all penalties for pilotPayoutId \"([^\"]*)\" for \"([^\"]*)\"$")
    public void viewPenaltiesComponentsWise(String payoutId, String transactionType) throws Throwable {
        String apiUrl = "/pilot/payout/" + payoutId + "/components";
        getRequestHeaders.put(CONTENT, APP);
        getRequestParameters.put(PAYOUTID, payoutId);
        getRequestParameters.put(TRANSANCTION, transactionType);
        response = CommonUtil.restUtil.getRequest(apiUrl, getRequestParameters, getRequestHeaders);
        System.out.println("response = " + response.asString());
        AssertionUtil.verifyEquals(response.path("response.pilotPayoutId").toString(), payoutId, "Not matched");
        dna = response.path("response.pilotPayoutComponentList.amount[0]").toString();
        lateReportingFromAPI = Double.valueOf(response.path("response.pilotPayoutComponentList.amount[1]").toString());
        notReported = response.path("response.pilotPayoutComponentList.amount[2]").toString();
       /* System.out.println( "DNA Amount for payoutId " + payoutId+  " is = " + dna );
        System.out.println( "Latereporting Amount for payoutId " + payoutId+  " is = " + lateReportingFromAPI );
        System.out.println( "Not Reported Amount for payoutId " + payoutId+  " is = " + notReported );
*/
    }

    @And("^View the \"([^\"]*)\" duration penalty of pilot \"([^\"]*)\" for \"([^\"]*)\" and period \"([^\"]*)\" and \"([^\"]*)\"$")
    public void viewDurationWisePenalty(String penalityType, String pilotId, String transactionType, String periodId, String payoutType) throws Throwable {
        String apiUrl = "/pilot/payout/components/breakup/durationwisepenalty";
        getRequestHeaders.put(CONTENT, APP);
        getRequestParameters.put(PILOTID, pilotId);
        getRequestParameters.put("periodId", periodId);
        getRequestParameters.put(PENALTYTYPE, penalityType);
        getRequestParameters.put(TRANSANCTION, transactionType);
        getRequestParameters.put(PAYOUTTYPE, payoutType);
        response = CommonUtil.restUtil.getRequest(apiUrl, getRequestParameters, getRequestHeaders);
        System.out.println("Response for " + penalityType + " is : " + response.asString());
        AssertionUtil.verifyEquals(response.path("status"), "SUCCESS", "success");
        penaltyAmount = response.path("response.amount");


    }

    @And("^DNA of pilotId \"([^\"]*)\" for \"([^\"]*)\"$")
    public void dutyNotAcceptedPenalties(String pilotId, String transactionType) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        String apiUrl = "/pilot/payout/components/breakup/dna";
        getRequestHeaders.put(CONTENT, APP);
        getRequestParameters.put(PILOTID, pilotId);
        getRequestParameters.put(PAYOUTTYPE, MONTHLY);
        getRequestParameters.put(TRANSANCTION, transactionType);
        response = CommonUtil.restUtil.getRequest(apiUrl, getRequestParameters, getRequestHeaders);
        System.out.println("Response of dna" + response.asString());
        AssertionUtil.verifyEquals(response.path("status"), "SUCCESS", "Done");
    }

    @And("^latereporting of pilotId \"([^\"]*)\" for periodID \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
    public void lateReporting(String pilotId, String periodId, String transactionType, String payoutType) throws Throwable {
        String apiUrl = "/pilot/payout/components/breakup/latereporting";
        getRequestHeaders.put(CONTENT, APP);
        getRequestParameters.put(PILOTID, pilotId);
        getRequestParameters.put(PERIOD, periodId);
        getRequestParameters.put(TRANSANCTION, transactionType);
        getRequestParameters.put(PAYOUTTYPE, payoutType);
        System.out.println(getRequestParameters);
        response = CommonUtil.restUtil.getRequest(apiUrl, getRequestParameters, getRequestHeaders);
        System.out.println(response.asString());
        //AssertionUtil.verifyEquals( response.path( "status" ), "SUCCESS", "Status is not matched" );
        //String lateReportingAmount = String.valueOf( response.path( "response[0].amount" ));
        //System.out.println( "lateReportingAmount = " + lateReportingAmount );
    }

    @Given("^Edit the Penalties for PilotPayoutId \"([^\"]*)\" for \"([^\"]*)\"$")
    public void editThePenaltiesOfAPilot(String payoutId, String transactionType) throws Throwable {
        String baseurl = CommonUtil.loadTestData.getString("pilotBaseUrl");
        String apiUrl = "pilot/payout/" + payoutId + "/components";
        String pathOfImage = "/home/rajneesh/Downloads/LIVEALERTSTICKETTYPEWISE.png";
        response = RestAssured.given().contentType("multipart/form-data")
                .header("user", "1234")
                .param(TRANSANCTION, transactionType)
                .formParam("components[0].componentCategory", "PENALTY")
                .formParam("components[0].componentName", "DUTY_NOT_ACCEPTED_PENALTY")
                .formParam("components[0].amount", "111")
                .formParam("components[0].updateReason", "Test")
                .formParam("components[0].deductFromTransaction", "TRANSACTION_1")
                .contentType("multipart/form-data")
                .multiPart("components[0].file", new File(pathOfImage))
                .formParam("components[1].componentCategory", "DEDUCTION")
                .formParam("components[1].componentName", "CASHBOOK")
                .formParam("components[1].amount", "33")
                .formParam("components[1].updateReason", "Test")
                .formParam("components[1].deductFromTransaction", "TRANSACTION_1")
                .contentType("multipart/form-data")
                .multiPart("components[1].file", new File(pathOfImage))
                .when().post(baseurl + apiUrl).then().statusCode(200).extract().response();
        System.out.println("Edit penalties response" + response.asString());
//        AssertionUtil.verifyEquals( response.path( "response.pilotPayoutId" ),40114,"done" );
        //      AssertionUtil.verifyEquals( response.path( "response.pilotPayoutComponentList.componentName[0]" ), "BASIC_SALARY", "success" );
    }


    @And("^Verify Penality amount from database for payoutId \"([^\"]*)\" and penalityType \"([^\"]*)\"$")
    public void verifyPenalityAmountFromDatabase(String payoutId, String penalityType) throws Throwable {
        String queryToVerifyPenaltiesComponentWise = "select amount from pilot_payout_component where pilot_payout_id = {payoutId} and component_name = '{penalityType}'";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String jsonResult = mysqlDbUtil.getJsonDataFromDb(queryToVerifyPenaltiesComponentWise.replace("{payoutId}", String.valueOf(payoutId)).replace("{penalityType}", String.valueOf(penalityType)));
        mysqlDbUtil.disconnect();
        TypeToken <List <Verification>> token = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(jsonResult, token.getType());
        List <String> dbResult = verificationList.stream().map(ver -> ver.getAmount()).collect(Collectors.toList());
        if (penalityType.equals("DUTY_NOT_ACCEPTED_PENALTY")) {
            AssertionUtil.verifyEquals(dna, dbResult.get(0), "Its not Matching");
            System.out.println("duty not accepted penalty from DB: " + dbResult);
            System.out.println("duty not accepted penalty from API: " + dna);
        } else if (penalityType.equals("LATE_REPORTED_PENALTY")) {
            lateReportingFromDB = Double.valueOf(String.valueOf(dbResult.get(0)));
            System.out.println("lateReportingFromDB = " + lateReportingFromDB);
//            AssertionUtil.verifyEquals( lateReportingFromAPI, dbResult.get( 0 ), "Late reporeted data is not matching" );
            System.out.println("latereporting penalty from DB: " + dbResult);
            System.out.println("latereporting penalty from API : " + lateReportingFromAPI);
        } else {
            AssertionUtil.verifyEquals(notReported, dbResult.get(0), "Not reported data is not matching");
            System.out.println("not reported penalty from DB: " + dbResult);
            System.out.println("not reported penalty from API: " + notReported);
        }
    }

    @Given("^Apply the below filters$")
    public void applyTheFilters(DataTable filters) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        List <List <String>> data = filters.raw();
        getRequestParameters.put(data.get(0).get(0), data.get(0).get(1));
        getRequestParameters.put(data.get(1).get(0), data.get(1).get(1));
        getRequestParameters.put(data.get(2).get(0), data.get(2).get(1));
        getRequestParameters.put(data.get(3).get(0), data.get(3).get(1));
        getRequestParameters.put(data.get(4).get(0), data.get(4).get(1));
        getRequestParameters.put(data.get(5).get(0), data.get(5).get(1));
        getRequestParameters.put(data.get(6).get(0), data.get(6).get(1));
        getRequestParameters.put(data.get(7).get(0), data.get(7).get(1));
        getRequestParameters.put(data.get(8).get(0), data.get(8).get(1));
        getRequestParameters.put(data.get(9).get(0), data.get(9).get(1));
        getRequestParameters.put(data.get(10).get(0), data.get(10).get(1));
        getRequestParameters.put(data.get(11).get(0), data.get(11).get(1));
        getRequestParameters.put(data.get(12).get(0), data.get(12).get(1));
        getRequestParameters.put(data.get(13).get(0), data.get(13).get(1));
        getRequestParameters.put(data.get(14).get(0), data.get(14).get(1));
        getRequestParameters.put(data.get(15).get(0), data.get(15).get(1));
        getRequestHeaders.put(USER, "1234");
        getRequestHeaders.put(CONTENT, APP);
        response = CommonUtil.restUtil.getRequest(URI, getRequestParameters, getRequestHeaders);
        AssertionUtil.verifyEquals(response.path("status"), "SUCCESS", "verifying response value");
        AssertionUtil.verifyEquals(response.path("errorMessage"), null, "done");
        System.out.println("response of all api is " + response.asString());
    }


    @When("^Verify \"([^\"]*)\" amount from database of pilot \"([^\"]*)\" for periodId \"([^\"]*)\"$")
    public void verifyDurationWisePenalityAmountFromDatabase(String penaltyType, String pilotId, String periodId) throws Throwable {
        String query = "select {penaltyType}*260 as amount from pilot_payout_metrics where pilot_id = {pilotId} and period_id ={periodId}";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String queryResult = mysqlDbUtil.getJsonDataFromDb(query.replace("{penaltyType}", String.valueOf(penaltyType)).replace("{pilotId}", String.valueOf(pilotId)).replace("{periodId}", String.valueOf(periodId)));
        mysqlDbUtil.disconnect();
        TypeToken <List <Verification>> token = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(queryResult, token.getType());
        List <String> dbResult = verificationList.stream().map(ver -> ver.getAmount()).collect(Collectors.toList());
        AssertionUtil.verifyEquals(dbResult, penaltyAmount, "Not matched");
        System.out.println("Not confirmed penalty from db is: " + dbResult + " and Nor confirmed penalty from api is: " + penaltyAmount);


    }

    @And("^Verify PayDays from database for pilotCode \"([^\"]*)\" and periodid \"([^\"]*)\"$")
    public void verifyPayDaysFromDatabase(String pilotId, String periodId) throws Throwable {
        String query = "select round((total_pay_days_in_millis/3600000/24),0) as payDays from pilot_payout_metrics where pilot_id = {pilotId} and period_id ={periodId}";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String queryResult = mysqlDbUtil.getJsonDataFromDb(query.replace("{pilotId}", String.valueOf(pilotId)).replace("{periodId}", String.valueOf(periodId)));
        mysqlDbUtil.disconnect();
        TypeToken <List <Verification>> token = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(queryResult, token.getType());
        List <String> dbResult = verificationList.stream().map(ver -> ver.getPayDays()).collect(Collectors.toList());
        AssertionUtil.verifyEquals(dbResult, payDays, "Not Matched");
    }

    @And("^Verify PayDays from database for above pilots and periodid \"([^\"]*)\"$")
    public void verifyPayDaysFromDatabase(String periodId) throws Throwable {
        String query = "select round((total_pay_days_in_millis/3600000/24),0) as payDays from pilot_payout_metrics where pilot_id in ({pilotId}) and period_id ={periodId}";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String queryResult = mysqlDbUtil.getJsonDataFromDb(query.replace("{pilotId}", String.valueOf(pilotIds)).replace("{periodId}", String.valueOf(periodId)));
        mysqlDbUtil.disconnect();
        System.out.println("payDays in verification method = " + pilotIds);
        TypeToken <List <Verification>> token = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(queryResult, token.getType());
        List <String> dbResult = verificationList.stream().map(ver -> ver.getPayDays()).collect(Collectors.toList());
        for (int i = 0; i < dbResult.size() - 1; i++) {
            // AssertionUtil.verifyEquals(  String.valueOf(Math.round(Double.parseDouble( dbResult.get(i)))), String.valueOf( payDays.get(i)), "Not matched" );
            System.out.println("Paydays from db: " + dbResult.get(i) + " And Paydays from API: " + payDays.get(i));
        }
    }

    public void checkIfPenaltyStartInPreviousMonth(String pilotId, String previousPeriodId) {
        Long startTime = null;
        String queryForLastMonth = "select pilot_state as pilotState, pilot_state_type as pilotStateType, start_timestamp as startTimestamp, expected_report_timestamp as expectedReportTime from pilot_payout_duty_report where pilot_id={pilotId} and period_id = {periodId} and is_active";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String queryResult = mysqlDbUtil.getJsonDataFromDb(queryForLastMonth.replace("{pilotId}", String.valueOf(pilotId)).replace("{periodId}", previousPeriodId));
        System.out.println(queryForLastMonth);
        mysqlDbUtil.disconnect();
        TypeToken <List <Verification>> token = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(queryResult, token.getType());
        for (int i = verificationList.size(); i >= 0; i--) {
            if (verificationList.get(i).getPilotStateType().equals("PENALTY") && (verificationList.get(i).getPilotStateType().equals("NOT_CONFIRMED") || verificationList.get(i).getPilotStateType().equals("NOT_REPORTED"))) {
                System.out.println("perioddate = " + perioddate.getStartDateOfPeriodId(previousPeriodId));
            }
            startTime = perioddate.getStartDateOfPeriodId(previousPeriodId);
        }
    }

    // Verification of calculation of penalties
    @And("^Verify DNA and NotReported calculation logic for pilotId \"([^\"]*)\" and periodId \"([^\"]*)\"$")
    public void verifyDurationWisePenaltiesCalculationLogic(String pilotId, String periodId) throws Throwable {
        Double notReportedDuration = 0.0;
        Double dnaDuration = 0.0;
        Double totalDNADuration = 0.0;
        Double totalNotReportedDuration = 0.0;
        String queryForThisMonth = "select id as id, pilot_state as pilotState, pilot_state_type as pilotStateType, start_timestamp as startTimestamp, expected_report_timestamp as expectedReportTime from pilot_payout_duty_report where pilot_id={pilotId} and period_id = {periodId} and is_active";
        String queryForLastMonth = "select pilot_state as pilotState, pilot_state_type as pilotStateType, start_timestamp as startTimestamp, expected_report_timestamp as expectedReportTime from pilot_payout_duty_report where pilot_id={pilotId} and period_id = ({periodId}-1) and is_active";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String queryResult = mysqlDbUtil.getJsonDataFromDb(queryForThisMonth.replace("{pilotId}", String.valueOf(pilotId)).replace("{periodId}", periodId));
        String queryResult1 = mysqlDbUtil.getJsonDataFromDb(queryForLastMonth.replace("{pilotId}", String.valueOf(pilotId)).replace("{periodId}", periodId));
        mysqlDbUtil.disconnect();
        //Checking for last month
        String penaltyType = "";
        TypeToken <List <Verification>> previousMonth = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList1 = new Gson().fromJson(queryResult1, previousMonth.getType());
        if (verificationList1.size() != 0) {
            for (int a = verificationList1.size() - 1; a > verificationList1.size() - 2; a--) {
                if (verificationList1.get(a).getPilotState().equals("INACTIVE")
                        || verificationList1.get(a).getPilotState().equals("PENALTY")
                        && (verificationList1.get(a).getPilotStateType().equals("NOT_CONFIRMED")
                        || verificationList1.get(a).getPilotStateType().equals("NOT_REPORTED"))) {
                    startTime = perioddate.getStartDateOfPeriodId(periodId);
                    isPenaltyStartedInLastMonth = 1;
                    penaltyType = verificationList1.get(a).getPilotStateType();
                }

                break;
            }
        }
        //  Checking for current month
        TypeToken <List <Verification>> currentMonth = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(queryResult, currentMonth.getType());
        int isPenaltyEnded = 0;
        for (int i = 0; i < verificationList.size(); i++) {
            if (isPenaltyEnded == 1) {
                break;
            }
            if (isPenaltyStartedInLastMonth == 1) {
                if ((verificationList.get(i).getPilotState().equals("DUTY")
                        || verificationList.get(i).getPilotState().equals("LEAVE")
                        || verificationList.get(i).getPilotState().equals("INACTIVE")
                        || verificationList.get(i).getPilotState().equals("PSEUDO_DUTY"))) {
                    endTime = Long.valueOf(verificationList.get(i).getStartTimestamp());
                    if (penaltyType != null && endTime != null && penaltyType.equals("NOT_CONFIRMED")) {
                        dnaDuration = Double.valueOf(endTime - startTime);
                        if (dnaDuration < 0) {
                            dnaDuration = 0d;
                        }
                        ;
                        totalDNADuration += dnaDuration;
                        isPenaltyStartedInLastMonth = 0;
                    }

                    if (penaltyType != null && endTime != null && penaltyType.equals("NOT_REPORTED")) {
                        notReportedDuration = Double.valueOf((endTime - startTime));
                        if (notReportedDuration < 0) {
                            notReportedDuration = 0d;
                        }
                        ;
                        totalNotReportedDuration += notReportedDuration;
                        isPenaltyStartedInLastMonth = 0;
                    }

                }

            }

            if (verificationList.get(i).getPilotState().equals("PENALTY")
                    && (verificationList.get(i).getPilotStateType().equals("NOT_CONFIRMED")
                    || verificationList.get(i).getPilotStateType().equals("NOT_REPORTED"))) {
                if (isPenaltyStartedInLastMonth == 1 && penaltyType != null) {
                    startTime = perioddate.getStartDateOfPeriodId(periodId);
                } else {
                    startTime = verificationList.get(i).getExpectedReportTime();
                }
                int isPenaltyEndedInSameMonth = 0;
                for (int j = i + 1; j < verificationList.size(); j++) {
                    if (verificationList.get(j).getPilotState().equals("DUTY")
                            || verificationList.get(j).getPilotState().equals("LEAVE")
                            || verificationList.get(j).getPilotState().equals("INACTIVE")
                            || verificationList.get(j).getPilotState().equals("PSEUDO_DUTY")) {
                        isPenaltyEndedInSameMonth = 1;
                        //Calculating endtime of penalty
                        endTime = Long.valueOf(verificationList.get(j).getStartTimestamp());
                        if (endTime != null && verificationList.get(i).getPilotStateType().equals("NOT_CONFIRMED")) {
                            dnaDuration = Double.valueOf(endTime - startTime);
                            if (dnaDuration < 0) {
                                dnaDuration = 0d;
                            }
                            totalDNADuration += dnaDuration;
                        }

                        if (endTime != null && verificationList.get(i).getPilotStateType().equals("NOT_REPORTED")) {
                            notReportedDuration = Double.valueOf((endTime - startTime));
                            if (notReportedDuration < 0) {
                                notReportedDuration = 0d;
                            }
                            totalNotReportedDuration += notReportedDuration;
                        }
                        i = j;
                        break;
                    }
                }
                if (isPenaltyEndedInSameMonth == 0 && verificationList.get(i).getPilotStateType().equals("NOT_CONFIRMED")) {
                    endTime = perioddate.getEndDateOfPeriodId(periodId);
                    dnaDuration = Double.valueOf(endTime - startTime);
                    if (dnaDuration < 0) {
                        dnaDuration = 0d;
                    }
                    ;
                    totalDNADuration += dnaDuration;
                    break;
                }
                if (isPenaltyEndedInSameMonth == 0 && verificationList.get(i).getPilotStateType().equals("NOT_REPORTED")) {
                    endTime = perioddate.getEndDateOfPeriodId(periodId);
                    notReportedDuration = Double.valueOf((endTime - startTime));
                    if (notReportedDuration < 0) {
                        notReportedDuration = 0d;
                    }
                    ;
                    totalNotReportedDuration += notReportedDuration;
                    break;
                }
            }
        }
        if (totalDNADuration > 0 && totalDNADuration < 10800000) {
            dnaPenaltyAmount = Double.valueOf(230);
        } else {
            dnaPenaltyAmount = Double.valueOf(Math.round(totalDNADuration / 3600000 / 6) * 230);
        }
        if (totalNotReportedDuration > 0 && totalNotReportedDuration < 10800000) {
            notReportedPenaltyAmount = Double.valueOf(230);
        } else {
            notReportedPenaltyAmount = Double.valueOf(Math.round(totalNotReportedDuration / 3600000 / 6) * 230);
        }
       /* System.out.println( "DNA penalty Amount for : " + pilotId + " is : " + dnaPenaltyAmount );
        System.out.println( "Not Reported Penalty for : " + pilotId + " is : " + notReportedPenaltyAmount );*/
    }

    @And("^Verify LateReported penalty calculation for pilotId \"([^\"]*)\" and periodId \"([^\"]*)\"$")
    public void verifyLateReportedPenaltyCalculation(String pilotid, String periodId) throws Throwable {
        String query = " select count(id)*230 as amount from pilot_payout_duty_report where pilot_id = {pilotId} and period_id = {periodId} and pilot_state_type ='LATE_REPORTED' and is_active";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance("rivigo", DbHosts.STAGING, false);
        String dbResult = mysqlDbUtil.getJsonDataFromDb(query.replace("{pilotId}", String.valueOf(pilotid)).replace("{periodId}", String.valueOf(periodId)));
        mysqlDbUtil.disconnect();
        TypeToken <List <Verification>> token = new TypeToken <List <Verification>>() {
        };
        List <Verification> verificationList = new Gson().fromJson(dbResult, token.getType());
        List <String> penalty = verificationList.stream().map(ver -> ver.getAmount()).collect(Collectors.toList());
        AssertionUtil.verifyEquals(Double.valueOf(penalty.get(0)), Double.valueOf(lateReportingFromAPI), "Not matching");
        System.out.println("late reported from db is: " + Double.valueOf(penalty.get(0)));
        System.out.println("Late reported amount from API is: " + Double.valueOf(lateReportingFromAPI));
    }

    @Given("^Go to pilot payout page for all and payoutPeriodId \"([^\"]*)\"$")
    public void goToPilotPayoutPageForAll(String periodId) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        String apiUrl = "/pilot/payout/all";
        getRequestHeaders.put(USER, "1234");
        getRequestHeaders.put(CONTENT, APP);
        getRequestParameters.put(PAYOUTTYPE, MONTHLY);
        getRequestParameters.put(PAYOUTSTATUS, "NEW");
        getRequestParameters.put(PERIOD, periodId);
        response = CommonUtil.restUtil.getRequest(apiUrl, getRequestParameters, getRequestHeaders);
        AssertionUtil.verifyEquals(response.path(STATUS), SUCCESS, "Done");
        AssertionUtil.verifyEquals(response.path(ERROR), null, "done");
    }

    @And("^apply active filter as \"([^\"]*)\" for payoutPeriodId \"([^\"]*)\" and \"([^\"]*)\"$")
    public void applyActivePilotsFilter(String isActive, String periodId, String transactionType) throws Throwable {
        getRequestParameters.put(TRANSANCTION, transactionType);
        getRequestParameters.put(PAYOUTTYPE, MONTHLY);
        getRequestParameters.put(PERIOD, periodId);
        getRequestParameters.put("isActive", isActive);
        getRequestParameters.put(PAYOUTSTATUS, "");
        getRequestHeaders.put(USER, "1234");
        getRequestHeaders.put(CONTENT, APP);
        response = CommonUtil.restUtil.getRequest(URI, getRequestParameters, getRequestHeaders);
    }

    @Given("^Download the report of \"([^\"]*)\" pilots for \"([^\"]*)\" and PeriodId \"([^\"]*)\"$")
    public void downloadThePilotPayutReportOfPilots(String status, String transactionType, String periodId) throws Throwable {
        String apiUrl = "/pilot/payout/transaction/report";
        getRequestHeaders.put(CONTENT, APP);
        getRequestHeaders.put(USER, "1223");
        getRequestParameters.put(PERIOD, periodId);
        getRequestParameters.put(PAYOUTSTATUS, status);
        getRequestParameters.put(TRANSANCTION, transactionType);
        getRequestParameters.put(PAYOUTTYPE, MONTHLY);
        response = CommonUtil.restUtil.getRequest(apiUrl, getRequestParameters, getRequestHeaders);
        AssertionUtil.verifyEquals(response.path(STATUS), SUCCESS, "Done");
        AssertionUtil.verifyEquals(response.path(ERROR), null, "done");

    }

    @Given("^Apply filter zero trips in last seven days for payout period \"([^\"]*)\" and \"([^\"]*)\"$")
    public void applyFilterZeroTripsInLastDays(String periodId, String transactionType) throws Throwable {
        String zeroTripsStartTime = CommonUtil.loadTestData.getString("zeroTripsStartTime");
        String zeroTripsEndTime = CommonUtil.loadTestData.getString("zeroTripsEndTime");
        getRequestHeaders.put(CONTENT, APP);
        getRequestHeaders.put(USER, "2112");
        getRequestParameters.put(PERIOD, periodId);
        getRequestParameters.put(TRANSANCTION, transactionType);
        getRequestParameters.put(PAYOUTTYPE, MONTHLY);
        getRequestParameters.put("zeroTripsStartTime", zeroTripsStartTime);
        getRequestParameters.put("zeroTripsEndTime", zeroTripsEndTime);
        response = CommonUtil.restUtil.getRequest(URI, getRequestParameters, getRequestHeaders);
        AssertionUtil.verifyEquals(response.path("status"), "SUCCESS", "Failed");
        AssertionUtil.verifyEquals(response.path("errorMessage"), null, "Getting error");

    }

    @When("^Check the summary of filters \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and PeriodId \"([^\"]*)\"$")
    public void checkTheSummary(String status, String transactionType, String payoutType, String periodId) throws Throwable {
        String apiUrl = "/pilot/payout/transaction/all/summary";
        getRequestHeaders.put(CONTENT, APP);
        getRequestHeaders.put(USER, "232");
        getRequestParameters.put(PERIOD, periodId);
        getRequestParameters.put(PAYOUTSTATUS, status);
        getRequestParameters.put(TRANSANCTION, transactionType);
        getRequestParameters.put(PAYOUTTYPE, payoutType);
        response = CommonUtil.restUtil.getRequest(apiUrl, getRequestParameters, getRequestHeaders);
    }

}
