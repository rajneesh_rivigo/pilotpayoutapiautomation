package steps;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dto.Verification;
import enums.DbHosts;
import utils.MysqlDbUtil;

import java.util.List;
import java.util.stream.Collectors;

public class GetStartAndEndDateOfAPeriod {
    public Long getEndDateOfPeriodId(String periodId) {
        String enddate = null;
        String query = "select end_timestamp as endTimestamp from pilot_payout_period where id ={periodId}";
        MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance( "rivigo", DbHosts.STAGING, false );
        String queryResult = mysqlDbUtil.getJsonDataFromDb( query.replace( "{periodId}", String.valueOf( periodId ) ) );
        mysqlDbUtil.disconnect();
        TypeToken<List<Verification>> token = new TypeToken<List<Verification>>() {
        };
        List<Verification> verificationList = new Gson().fromJson( queryResult, token.getType() );
        List<String> dbResult = verificationList.stream().map( ver -> ver.getEndTimestamp() ).collect( Collectors.toList() );
        for (int i = 0; i < dbResult.size(); i++) {
            enddate = dbResult.get( i );
        }
        return Long.valueOf( enddate );
    }

        public Long getStartDateOfPeriodId(String periodId) {
            String startdate = null;
            String query = "select start_timestamp as startTimestamp from pilot_payout_period where id ={periodId}";
            MysqlDbUtil mysqlDbUtil = MysqlDbUtil.getInstance( "rivigo", DbHosts.STAGING, false );
            String queryResult = mysqlDbUtil.getJsonDataFromDb( query.replace( "{periodId}", String.valueOf( periodId ) ) );
            mysqlDbUtil.disconnect();
            TypeToken<List<Verification>> token = new TypeToken<List<Verification>>() {
            };
            List<Verification> verificationList = new Gson().fromJson( queryResult, token.getType() );
            List<String> dbResult = verificationList.stream().map( ver -> ver.getStartTimestamp() ).collect( Collectors.toList() );
            for (int i = 0; i < dbResult.size(); i++) {
                startdate = dbResult.get( i );
            }
            return Long.valueOf( startdate );
        }

}

