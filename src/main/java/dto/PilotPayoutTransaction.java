package dto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.joda.time.DateTime;
import javax.persistence.*;/**
 * Created by priyanshi on 7/12/17.
 */@Getter
@Setter
@Entity
@Table(name = "pilot_payout_transaction")
@ToString
@NoArgsConstructor
public class PilotPayoutTransaction {    @Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;
@Column(name = "pilot_payout_id")
private Long pilotPayoutId;
@Column(name = "total_amount")
private Double totalAmount;
@Enumerated(EnumType.STRING)
@Column(name = "status")
private String status;
@Enumerated(EnumType.STRING)
@Column(name = "transaction_type")
private String pilotPayoutTransactionType;
@Column(name = "processed_timestamp")
private DateTime processedDate;
}
