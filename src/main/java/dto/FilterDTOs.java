package dto;

public class FilterDTOs {
    private int payoutPeriodId;
    private String status;
    private int partialPilotCode;
    private String transactionType;
    private String payoutType;

    public int getPayoutPeriodId() {
        return payoutPeriodId;
    }

    public void setPayoutPeriodId(int payoutPeriodId) {
        this.payoutPeriodId = payoutPeriodId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPartialPilotCode() {
        return partialPilotCode;
    }

    public void setPartialPilotCode(int partialPilotCode) {
        this.partialPilotCode = partialPilotCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }

}
