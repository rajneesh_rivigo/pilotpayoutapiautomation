package dto;

public class Verification {

    String status;
    String amount;
    String totalAmount;
    String payDays;
    String id;
    String pilotState;
    String pilotStateType;
    String startTimestamp;

    public String getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(String endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public void setExpectedReportTime(String expectedReportTime) {
        this.expectedReportTime = expectedReportTime;
    }

    String endTimestamp;
    String expectedReportTime;
    public Long getExpectedReportTime(){
        return Long.valueOf( expectedReportTime);
    }
    public void setExpectedReportTime(){
        this.expectedReportTime = expectedReportTime;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPilotState() {
        return pilotState;
    }

    public void setPilotState(String pilotState) {
        this.pilotState = pilotState;
    }

    public String getPilotStateType() {
        return pilotStateType;
    }

    public void setPilotStateType(String pilotStateType) {
        this.pilotStateType = pilotStateType;
    }

    public String getStartTimestamp() {
        return  startTimestamp ;
    }

    public void setStartTimestamp(String startTimestamp) {
        this.startTimestamp = startTimestamp;
    }
    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPayDays() {
        return payDays;
    }

    public void setPayDays(String payDays) {
        this.payDays = payDays;
    }

}
