package dto;

public class Summary {


    private String payoutPeriodId;
    private String status;
    private String minTrips;
    private String maxTrips;
    private String isActive;
    private String transactionType;
    private String payoutType;


    public String getPayoutPeriodId() {
        return payoutPeriodId;
    }

    public void setPayoutPeriodId(String payoutPeriodId) {
        this.payoutPeriodId = payoutPeriodId;
    }
    public String getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMinTrips() {
        return minTrips;
    }

    public void setMinTrips(String minTrips) {
        this.minTrips = minTrips;
    }

    public String getMaxTrips() {
        return maxTrips;
    }

    public void setMaxTrips(String maxTrips) {
        this.maxTrips = maxTrips;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }




}
