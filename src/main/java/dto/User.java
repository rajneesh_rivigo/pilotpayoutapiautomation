package dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "User")
@ToString
public class User {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "emp_code", unique = true)
    private String empCode;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "contact_no", unique = true)
    private String contactNo;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "password")
    private String password;

    @Column(name = "current_location")
    private String currentLocation;

    @Column(name = "role")
    private Integer role;

    @Column(name = "designation")
    private String designation;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "access_hubs")
    private String accessHubs;

    @Column(nullable = false, name = "is_active")
    private Boolean isActive;

}
