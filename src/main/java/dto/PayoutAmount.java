package dto;

public class PayoutAmount {
    private String pilotPayoutId;
    private Long payoutAmount ;
    private String pilotPayoutPeriodId;

    public String getPilotPayoutPeriodId() {
        return pilotPayoutPeriodId;
    }

    public void setPilotPayoutPeriodId(String pilotPayoutPeriodId) {
        this.pilotPayoutPeriodId = pilotPayoutPeriodId;
    }



    public String getPilotPayoutId() {
        return pilotPayoutId;
    }

    public void setPilotPayoutId(String pilotPayoutId) {
        this.pilotPayoutId = pilotPayoutId;
    }

    public Long getPayoutAmount() {
        return payoutAmount;
    }

    public void setPayoutAmount(Long payoutAmount) {
        this.payoutAmount = payoutAmount;
    }
}
