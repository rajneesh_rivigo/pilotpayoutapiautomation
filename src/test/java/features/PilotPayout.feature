Feature: Pilot Payout
  #-------------------------------Verify all the penalties-----------------------------------------------

  @VerifyPenalties
  Scenario: Verify all the penalties calculation, like DNA, LateReporting, Not Reported penalties for all the pilots
    Given apply "TRANSACTION_1" and "PROCESSED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10" for verifying penalties

  # -------------------- phase 1 payout processing for a specified pilot ---------------------------------

  @SearchPilot1
  Scenario: Search by partial pilot code, verify the status of payout and amount from database
  and approve the payout of the pilot and after status change again verify the status of the payout for specified phase

    Given Search for pilot code "6909" for "TRANSACTION_1" with status "NEW" and payout type "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "NEW" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    And View the all penalties for pilotPayoutId "9898989" for "TRANSACTION_1"
    And Verify DNA and NotReported calculation logic for pilotId "113485431" and periodId "10"
    And Verify LateReported penalty calculation for pilotId "113485445" and periodId "10"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"

  @SearchPilot1
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_1" with status "PET_APPROVED" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PET_HEAD_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"

  @SearchPilot1
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_1" with status "PET_HEAD_APPROVED" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "FINANCE_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"

  @SearchPilot1
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_1" with status "FINANCE_APPROVED" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PRIME_CEO_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_1"

  @SearchPilot1
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_1" with status "PRIME_CEO_APPROVED" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PROCESSED" for "TRANSACTION_1"
    And Verify Payout status from database is "PROCESSED" and "TRANSACTION_1"



  # -------------------- phase 2 payout processing for a specified pilot ---------------------------------

  @SearchPilot2
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_2" with status "NEW" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "NEW" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_2"

  @SearchPilot2
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_2" with status "PET_APPROVED" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PET_HEAD_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_2"

  @SearchPilot2
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_2" with status "PET_HEAD_APPROVED" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "FINANCE_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_2"

  @SearchPilot2
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_2" with status "FINANCE_APPROVED" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PRIME_CEO_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_2"

  @SearchPilot2
  Scenario: Search by partial pilot code and approve the payout of the pilot for specified phase
    Given Search for pilot code "8403" for "TRANSACTION_2" with status "PRIME_CEO_APPROVED" and payout type "MONTHLY_PAYOUT" and PeriodId "9"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PROCESSED" for "TRANSACTION_1"
    And Verify Payout status from database is "PROCESSED" and "TRANSACTION_1"
# -------------------- phase 1 payout processing ---------------------------------
  @Phase12
  Scenario: As a PET_TEAM, APPROVE the payout of phase_1 for a month's payout and verify the STATUS, AMOUNT, PAYDAYS,
  DNA and NOT REPORTED penalties calculation
    Given apply "TRANSACTION_1" and "NEW" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Verify Payout status from database is "NEW" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"

  @Phase1
  Scenario: As a PET_TEAM_HEAD, APPROVE the payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PET_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PET_HEAD_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"


  @Phase1
  Scenario: As a FINANCE_TEAM, APPROVE the payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PET_HEAD_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_HEAD_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "FINANCE_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"

  @Phase1
  Scenario: As a PRIME_CEO, APPROVE the payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "FINANCE_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "FINANCE_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PRIME_CEO_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_1"

  @Phase1
  Scenario: As a FINANCE TEAM, PROCESS the payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PRIME_CEO_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PRIME_CEO_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PROCESSED" for "TRANSACTION_1"
    And Verify Payout status from database is "PROCESSED" and "TRANSACTION_1"

    # -------------------- phase 2 payout processing ---------------------------------

  @Phase2
  Scenario: As a PET_TEAM, APPROVE the payout of phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "NEW" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "NEW" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "NEW" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_2"

  @Phase2
  Scenario: As a PET_TEAM_HEAD, APPROVE the payout of phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PET_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PET_HEAD_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_2"

  @Phase2
  Scenario: As a FINANCE_TEAM, APPROVE the payout of phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PET_HEAD_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_HEAD_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "FINANCE_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_2"

  @Phase2
  Scenario: As a PRIME_CEO, APPROVE the payout of phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "FINANCE_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "FINANCE_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PRIME_CEO_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_2"

  @Phase2
  Scenario: As a FINANCE TEAM, PROCESS the payout of phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PRIME_CEO_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PRIME_CEO_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PROCESSED" for "TRANSACTION_2"
    And Verify Payout status from database is "PROCESSED" and "TRANSACTION_2"

   # ------------------- FNF Phase 1 payout processing -------------------

  @FNF1
  Scenario: As a PET_TEAM, APPROVE the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "NEW" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "NEW" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "NEW" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"

  @FNF1
  Scenario: As a PET_TEAM_HEAD, APPROVE the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PET_APPROVED" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_APPROVED" and "TRANSACTION_" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PET_HEAD_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"

  @FNF1
  Scenario: As a FINANCE_TEAM, APPROVE the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PET_HEAD_APPROVED" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_HEAD_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "FINANCE_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"

  @FNF1
  Scenario: As a PRIME_CEO, APPROVE the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "FINANCE_APPROVED" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "FINANCE_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PRIME_CEO_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_1"

  @FNF1
  Scenario: As a FINANCE TEAM, PROCESS the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PRIME_CEO_APPROVED" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PRIME_CEO_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PROCESSED" for "TRANSACTION_1"
    And Verify Payout status from database is "PROCESSED" and "TRANSACTION_1"

    # ------------------- FNF Phase 2 payout processing -------------------

  @FNF2
  Scenario: As a PET_TEAM, APPROVE the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "NEW" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "NEW" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "NEW" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_2"

  @FNF2
  Scenario: As a PET_TEAM_HEAD, APPROVE the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PET_APPROVED" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PET_HEAD_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_2"

  @FNF2
  Scenario: As a FINANCE_TEAM, APPROVE the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PET_HEAD_APPROVED" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_HEAD_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "FINANCE_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_2"

  @FNF2
  Scenario: As a PRIME_CEO, APPROVE the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "FINANCE_APPROVED" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "FINANCE_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PRIME_CEO_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_2"

  @FNF2
  Scenario: As a FINANCE TEAM, PROCESS the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PRIME_CEO_APPROVED" filters for "FNF_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PRIME_CEO_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PROCESSED" for "TRANSACTION_2"
    And Verify Payout status from database is "PROCESSED" and "TRANSACTION_2"

 # ------------------- PENDING_HOLD_PAYOUT Phase 1 payout processing -------------------

  @HOLD1
  Scenario: As a PET_TEAM, APPROVE the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "NEW" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "NEW" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "NEW" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"

  @HOLD1
  Scenario: As a PET_TEAM_HEAD, APPROVE the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PET_APPROVED" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PET_HEAD_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"

  @HOLD1
  Scenario: As a FINANCE_TEAM, APPROVE the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PET_HEAD_APPROVED" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_HEAD_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "FINANCE_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"

  @HOLD1
  Scenario: As a PRIME_CEO, APPROVE the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "FINANCE_APPROVED" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "FINANCE_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PRIME_CEO_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_1"

  @HOLD1
  Scenario: As a FINANCE TEAM, PROCESS the payout of FNF phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PRIME_CEO_APPROVED" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PRIME_CEO_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PROCESSED" for "TRANSACTION_1"
    And Verify Payout status from database is "PROCESSED" and "TRANSACTION_1"

    # ------------------- PENDING_HOLD_PAYOUT Phase 2 payout processing -------------------

  @HOLD2
  Scenario: As a PET_TEAM, APPROVE the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "NEW" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "NEW" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "NEW" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_2"

  @HOLD2
  Scenario: As a PET_TEAM_HEAD, APPROVE the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PET_APPROVED" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PET_HEAD_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_2"

  @HOLD2
  Scenario: As a FINANCE_TEAM, APPROVE the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PET_HEAD_APPROVED" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_HEAD_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "FINANCE_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_2"

  @HOLD2
  Scenario: As a PRIME_CEO, APPROVE the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "FINANCE_APPROVED" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "FINANCE_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PRIME_CEO_APPROVED" for "TRANSACTION_2"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_2"

  @HOLD2
  Scenario: As a FINANCE TEAM, PROCESS the payout of FNF phase_2 for June month's payout
    Given apply "TRANSACTION_2" and "PRIME_CEO_APPROVED" filters for "PENDING_HOLD_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PRIME_CEO_APPROVED" and "TRANSACTION_2" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PRIME_CEO_APPROVED" and "TRANSACTION_2"
    And Verify Payout amount from database for "TRANSACTION_2"
    Then For above Pilot_payout_ids change status to "PROCESSED" for "TRANSACTION_2"
    And Verify Payout status from database is "PROCESSED" and "TRANSACTION_2"

    # -------------------- phase 1 payout Hold ---------------------------------
  @auto3
  Scenario: As a PET_TEAM, Hold the payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "NEW" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "NEW" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "NEW" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "HOLD" for "TRANSACTION_1"
    And Verify Payout status from database is "HOLD" and "TRANSACTION_1"

  @auto3
  Scenario: As a PET_TEAM_HEAD, HOLD the payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PET_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "HOLD" for "TRANSACTION_1"
    And Verify Payout status from database is "HOLD" and "TRANSACTION_1"

  @auto3
  Scenario: As a FINANCE_TEAM, HOLD the payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "PET_HEAD_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "PET_HEAD_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "PET_HEAD_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "HOLD" for "TRANSACTION_1"
    And Verify Payout status from database is "HOLD" and "TRANSACTION_1"

  @auto3
  Scenario: As a PRIME_CEO, HOLD the payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "FINANCE_APPROVED" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "FINANCE_APPROVED" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "FINANCE_APPROVED" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "HOLD" for "TRANSACTION_1"
    And Verify Payout status from database is "HOLD" and "TRANSACTION_1"

  @auto3
  Scenario: As a PET_HEAD, Approve the Hold payout of phase_1 for month's payout
    Given apply "TRANSACTION_1" and "HOLD" filters for "MONTHLY_PAYOUT" payout of PeriodId "10"
    When Check the summary of filters "HOLD" and "TRANSACTION_1" and "MONTHLY_PAYOUT" and PeriodId "10"
    And Verify Payout status from database is "HOLD" and "TRANSACTION_1"
    And Verify Payout amount from database for "TRANSACTION_1"
    Then For above Pilot_payout_ids change status to "PET_APPROVED" for "TRANSACTION_1"
    And Verify Payout status from database is "PET_APPROVED" and "TRANSACTION_1"

    #-------------------------------------------------------------------------------------------------

  @viewPenalties
 Scenario: View all the Penalties for a perticular pilot
    #Given View the all penalties for pilotPayoutId "59840" for "TRANSACTION_1"
    #And Verify Penality amount from database for payoutId "53939" and penalityType "DUTY_NOT_ACCEPTED_PENALTY"
    And Verify Penality amount from database for payoutId "59066" and penalityType "LATE_REPORTED_PENALTY"
    #And Verify Penality amount from database for payoutId "53939" and penalityType "NOT_REPORTED_PENALTY"
   # And View the "NOT_CONFIRMED" duration penalty of pilot "690441963" for "TRANSACTION_1" and period "9" and "MONTHLY_PAYOUT"
    #When Verify "total_not_confirmed_penalty" amount from database of pilot "690441963" for periodId "9"
    #And View the "NOT_REPORTED" duration penalty of pilot "690441963" for "TRANSACTION_1" and period "9" and "MONTHLY_PAYOUT"
    #When Verify "total_not_reported_penalty" amount from database of pilot "690441963" for periodId "9"


  @edit
  Scenario: Edit the Penalties
    Given Edit the Penalties for PilotPayoutId "40114" for "TRANSACTION_1"
   # And DNA of pilotId "6463585" for "TRANSACTION_1"
    And latereporting of pilotId "6463585" for periodID "10", "TRANSACTION_1" and "MONTHLY_PAYOUT"

  @downloadReport
  Scenario: Download the pilot payout report of hold pilots
    Given Download the report of "HOLD" pilots for "TRANSACTION_1" and PeriodId "7"


  @zeroToSeven
  Scenario: Check zero trips in last 7 days filter's functionality
    Given Apply filter zero trips in last seven days for payout period "6" and "TRANSACTION_1"



  @filters
  Scenario: Payout for all Pilots for a specified filters should be visible
    Given  Apply the below filters
      | hubCode |  |
      | transactionType | TRANSACTION_1 |
      | payoutPeriodId | 6 |
      | status         |   |
      | partialPilotCode |   |
      | isActive |   |
      | minTrips  |   |
      | maxTrips   |   |
      | page         |   |
      | sortColumn   |   |
      | sortOrder   |   |
      | isBankDetailsUpdated   |   |
      | isPancardDetailsUpdated   |   |
      | zeroTripsStartTime   |   |
      | zeroTripsEndTime   |   |
      | payoutType | MONTHLY_PAYOUT |
      # When Check the summary "summary1"
    Then Payout for all eligible pilots should be shown

  @all
  Scenario: Check the inactive or active pilots for specified transaction type and specified payout period
    Given Go to pilot payout page for all and payoutPeriodId "7"
    And apply active filter as "false" for payoutPeriodId "7" and "TRANSACTION_1"
    #When Check inactive pilots in database
    Then Check the summary of "summary1"
