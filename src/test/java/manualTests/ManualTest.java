package manualTests;

import com.rivigo.Log;
import org.testng.annotations.Test;

public class ManualTest {

    @Test
    public static void manualTest() throws InterruptedException {
        Log.info("Manual testing session started");
        Thread.sleep(60000);
        Log.info("Manual testing session ended");
    }
}
