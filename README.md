# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains pilot auto allocation tests using Cucumber BDD.

### How do I get set up? ###

To set up on local:

1. Gradle is required to build the project.
2. Build prime-api-automation-commons project on your local (https://bitbucket.org/rivigotech/prime-api-automation-common)
3. Pilot and vehicle should be up on the system.
4. Import base mysql snapshot dump.
5. Run CucumberRunnerUtil class with parameter "environment" and value ("local" to run in local) to execute tests.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact